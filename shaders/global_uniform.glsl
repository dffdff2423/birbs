#ifndef GLOBAL_UNIFORM_GLSL
#define GLOBAL_UNIFORM_GLSL

layout (std140, binding = 0) uniform GlobalUniform {
	mat4 proj;
} g;

#endif // GLOBAL_UNIFORM_GLSL
