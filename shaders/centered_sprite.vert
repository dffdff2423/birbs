#version 450 core

#define PI_over_2 radians(90)

layout (location = 0) in vec3 i_vpos;

layout (location = 0) out vec3 o_color;

#include "global_uniform.glsl"

layout (std140, binding = 1) uniform ModelUniform {
	vec4 color_and_scale; // xyz = color, w = scale
	vec2 pos;
	vec2 velo; // for rotation
} m;

mat2 rot2d(float theta /* radians */)
{
	return mat2(
		cos(theta), -sin(theta),
		sin(theta),  cos(theta)
	);
}

void main()
{
	o_color = m.color_and_scale.xyz;

	float rot = 0.0;
	if (m.velo.y != 0)
		rot = atan(m.velo.y, m.velo.x);
	mat2 rmat = rot2d(PI_over_2 - rot);
	vec2 vpos_xy = rmat * i_vpos.xy;
	vec3 vpos = vec3(vpos_xy, i_vpos.z);
	gl_Position = g.proj * vec4(vpos * m.color_and_scale.w + vec3(m.pos, 0.0), 1.0);
}
