#ifndef BIRBS_WINDOW_HH
#define BIRBS_WINDOW_HH

#include <stdexcept>
#include <utility>

#include "birbs/glm_defs.hh"
#include <glm/ext/vector_uint2_sized.hpp>

#include "birbs/core.hh"

struct GLFWwindow;

namespace birbs {

struct GLFWError : public std::runtime_error {
	GLFWError(int code, const char *txt) : runtime_error(txt), m_code(code) {}
	int m_code;
};

/// Must be created on the main thread, and only 1 per program
class WindowingCTX : private NoCopy, NoMove {
public:
	static WindowingCTX init();
	~WindowingCTX();

private:
	WindowingCTX() = default;
};

class Window : private NoCopy {
public:
	Window(WindowingCTX &, glm::u32vec2 size, const char *title);
	~Window();

	Window(Window &&rhs) noexcept
		: m_handle(rhs.m_handle)
	{
		if (&rhs != this) {
			rhs.m_handle = nullptr;
		}
		set_callback_ptr();
	}

	Window &operator=(Window &&rhs) noexcept
	{
		auto n = Window(std::move(rhs));
		std::swap(*this, n);
		return *this;
	}

	bool should_close() const noexcept;
	void set_close(bool val = true) const noexcept;
	void make_current() const noexcept;
	void swap_buffers() const noexcept;
	void poll() const noexcept;
	glm::uvec2 size() const noexcept;

private:
	void set_callback_ptr() noexcept;

	GLFWwindow *m_handle;
};

} // namespace birbs

#endif // BIRBS_WINDOW_HH
