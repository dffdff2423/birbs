#ifndef BIRBS_GL_HH
#define BIRBS_GL_HH

#include <span>
#include <stdexcept>
#include <type_traits>
#include <utility>

#include <glad/gl.h>

#include "birbs/core.hh"
#include "birbs/log.hh"

namespace birbs {

/// Loads opengl using GLAD. The context must be current before doing so.
void load_gl();

struct VertexAttribute {
	GLuint index;
	GLint size; // number of elements in vector
	GLint type;
	GLboolean normalize;
	GLuint offset;
};

template <typename T>
concept Vertex = std::is_standard_layout_v<T> && std::is_trivial_v<T>
		&& requires(T) {
	{ T::get_attrs() } -> std::same_as<std::span<const VertexAttribute>>;
};

// TODO: Index buffer
template<Vertex V>
class VertexBuffer : private NoCopy {
public:
	VertexBuffer(std::span<V> vertices);
	~VertexBuffer();

	VertexBuffer(VertexBuffer &&rhs) noexcept
		: m_vbo_handle(rhs.m_vbo_handle), m_vao_handle(rhs.m_vao_handle)
		, m_size(rhs.m_size)
	{
		if (this != &rhs) {
			rhs.m_vbo_handle = 0;
			rhs.m_vao_handle = 0;
			rhs.m_size = 0;
		}
	}

	VertexBuffer &operator=(VertexBuffer &&rhs) noexcept
	{
		VertexBuffer n = VertexBuffer(std::move(rhs));
		std::swap(*this, n);
		return *this;
	}

	void bind_vao() const noexcept;
	size_t size() const noexcept { return m_size; }

private:
	GLuint m_vbo_handle, m_vao_handle;
	size_t m_size;
};

template<Vertex V>
VertexBuffer<V>::VertexBuffer(std::span<V> vertices)
{
	glCreateVertexArrays(1, &m_vao_handle);

	glCreateBuffers(1, &m_vbo_handle);
	glNamedBufferStorage(m_vbo_handle, vertices.size_bytes(), vertices.data(), 0);

	glVertexArrayVertexBuffer(m_vao_handle, 0, m_vbo_handle, 0, sizeof(V));

	auto attrs = V::get_attrs();
	for (auto &attr : attrs) {
		glEnableVertexArrayAttrib(m_vao_handle, attr.index);
		glVertexArrayAttribFormat(m_vao_handle, attr.index, attr.size
				, attr.type, attr.normalize, attr.offset);
		glVertexArrayAttribBinding(m_vao_handle, attr.index, 0);
	}

	m_size = vertices.size();
}

template<Vertex V>
VertexBuffer<V>::~VertexBuffer()
{
	glDeleteBuffers(1, &m_vbo_handle);
	glDeleteVertexArrays(1, &m_vao_handle);
}

template<Vertex V>
void VertexBuffer<V>::bind_vao() const noexcept
{
	glBindVertexArray(m_vao_handle);
}

template<typename T>
concept UniformBufferContents = std::is_standard_layout_v<T>
		&& std::is_trivial_v<T> && sizeof(T) < 16*units::KB;
// Because we don't check the max uniform size at runtime it must be less than 16 KiB

template<UniformBufferContents U>
class UniformBuffer : private NoCopy {
public:
	class MapGuard : private NoCopy {
	public:
		~MapGuard() noexcept { unmap(); }
		MapGuard(MapGuard &&rhs) noexcept
			:m_owner(rhs.m_owner), m_ptr(rhs.m_ptr), m_access(rhs.m_access)
		{
			if (this != &rhs) {
				rhs.m_ptr = nullptr;
				rhs.m_access = 0;
			}
		}

		MapGuard &operator=(MapGuard &&rhs) noexcept {
			MapGuard n = MapGuard(std::move(rhs));
			std::swap(*this, n);
			return *this;
		}

		const U &operator*() const {
			if (m_access == GL_WRITE_ONLY)
				throw std::runtime_error("Tried to read write-only opengl mapping");
			return *m_ptr;
		}
		const U *operator->() const {
			if (m_access == GL_WRITE_ONLY)
				throw std::runtime_error("Tried to read write-only opengl mapping");
			return m_ptr;
		}

		U &operator*() {
			if (m_access == GL_READ_ONLY)
				throw std::runtime_error("Tried to write read-only opengl mapping");
			return *m_ptr;
		}
		U *operator->() {
			if (m_access == GL_READ_ONLY)
				throw std::runtime_error("Tried to write read-only opengl mapping");
			return m_ptr;
		}

		void unmap() noexcept;

	private:
		friend class UniformBuffer<U>;

		MapGuard(const UniformBuffer<U> &owner, U *ptr, GLenum access)
			:m_owner(owner), m_ptr(ptr), m_access(access)
		{}

		const UniformBuffer &m_owner;
		U *m_ptr;
		GLenum m_access;
	};

	UniformBuffer();
	~UniformBuffer() noexcept;

	UniformBuffer(UniformBuffer &&rhs) noexcept
		:m_handle(rhs.m_handle)
	{
		if (this != &rhs) {
			rhs.m_handle = 0;
		}
	}

	UniformBuffer &operator=(UniformBuffer &&rhs) noexcept {
		UniformBuffer<U> n = UniformBuffer<U>(std::move(rhs));
		std::swap(*this, n);
		return *this;
	}

	MapGuard map(GLenum acesss) const noexcept;
	void bind_to(GLuint point) const noexcept;
private:
	friend void MapGuard::unmap() noexcept;

	GLuint m_handle;
};


template<UniformBufferContents U>
UniformBuffer<U>::UniformBuffer()
{
	glCreateBuffers(1, &m_handle);
	constexpr GLbitfield flags = GL_MAP_READ_BIT | GL_MAP_WRITE_BIT;
	glNamedBufferStorage(m_handle, sizeof(U), nullptr, flags);
}

template<UniformBufferContents U>
UniformBuffer<U>::~UniformBuffer() noexcept
{
	glDeleteBuffers(1, &m_handle);
}

template<UniformBufferContents U>
typename UniformBuffer<U>::MapGuard UniformBuffer<U>::map(GLenum access) const noexcept
{
	U *ptr = static_cast<U *>(glMapNamedBuffer(m_handle, access));
	return MapGuard(*this, ptr, access);
}

template<UniformBufferContents U>
void UniformBuffer<U>::MapGuard::unmap() noexcept
{
	if (m_ptr) {
		GLboolean res = glUnmapNamedBuffer(m_owner.m_handle);
		if (res != GL_TRUE) {
			LOG_e("glUnmapNamedBuffer returned GL_FALSE, does this even happen on modern OS?");
			std::abort();
		}
		m_access = 0;
		m_ptr = nullptr;
	}
}

template<UniformBufferContents U>
void UniformBuffer<U>::bind_to(GLenum point) const noexcept
{
	glBindBufferBase(GL_UNIFORM_BUFFER, point, m_handle);
}

class GraphicsShaderProgram : private NoCopy {
public:
	GraphicsShaderProgram(const char *vsh, const char *fsh);
	~GraphicsShaderProgram() noexcept;


	GraphicsShaderProgram(GraphicsShaderProgram &&rhs) noexcept
		: m_handle(rhs.m_handle)
	{
		if (this != &rhs) {
			rhs.m_handle = 0;
		}
	}

	GraphicsShaderProgram &operator=(GraphicsShaderProgram &&rhs) noexcept
	{
		GraphicsShaderProgram n = GraphicsShaderProgram(std::move(rhs));
		std::swap(*this, n);
		return *this;
	}

	void use() const noexcept;
private:
	GLuint m_handle;
};

template<Vertex V>
void draw_triangles(const VertexBuffer<V> &vbuf, const GraphicsShaderProgram &prog) noexcept
{
	prog.use();
	vbuf.bind_vao();
	glDrawArrays(GL_TRIANGLES, 0, vbuf.size());
}

} // namespace birbs

#endif // BIRBS_GL_HH
