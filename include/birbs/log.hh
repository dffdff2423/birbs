#ifndef BIRBS_LOG_HH
#define BIRBS_LOG_HH

#include <stdio.h>
#include <format>

#ifdef __unix__
#include <unistd.h>
#endif // __unix__

#include "birbs/core.hh"

namespace birbs {

enum class LogLevel : u8 {
	Debug, Info, Warn, Error,
};

constexpr LogLevel MIN_LOG_LEVEL = LogLevel::Debug;

#define LOG_e(...) ::birbs::_log(::birbs::LogLevel::Error, __func__, __VA_ARGS__)
#define LOG_w(...) ::birbs::_log(::birbs::LogLevel::Warn,  __func__, __VA_ARGS__)
#define LOG_i(...) ::birbs::_log(::birbs::LogLevel::Info,  __func__, __VA_ARGS__)
#define LOG_d(...) ::birbs::_log(::birbs::LogLevel::Debug, __func__, __VA_ARGS__)

template<class... Args>
void _log(LogLevel level, const char *func, std::format_string<Args...> fmt, Args&&... args)
{
	const char *level_txt;
	switch (level) {
	case LogLevel::Debug:
		level_txt = "DEBUG";
		break;
	case LogLevel::Info:
		level_txt = "INFO ";
		break;
	case LogLevel::Warn:
		level_txt = "WARN ";
		break;
	case LogLevel::Error:
		level_txt = "ERROR";
		break;
	}
	const char *ansi_code = "";
#	ifdef __unix__
	constexpr const char *ANSI_DEFAULT = "\x1b[39m";
	int fd = fileno(stderr);
	if (fd >= 0 && isatty(fd)) {
		switch (level) {
		case LogLevel::Debug:
			ansi_code = "\x1b[34m";
			break;
		case LogLevel::Info:
			ansi_code = "\x1b[32m";
			break;
		case LogLevel::Warn:
			ansi_code = "\x1b[33m";
			break;
		case LogLevel::Error:
			ansi_code = "\x1b[31m";
			break;
		}
	}
#	else
	constexpr const char *ANSI_DEFAULT = "";
#	endif // __unix__
	if (level >= MIN_LOG_LEVEL) {
		fprintf(stderr, "[%s%s%s %s]: %s\n", ansi_code, level_txt, ANSI_DEFAULT, func
				, std::format(fmt, std::forward<Args>(args)...).c_str());
	}
}

} // namespace birbs

#endif // BIRBS_LOG_HH
