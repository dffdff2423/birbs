#ifndef MATH_HH
#define MATH_HH

#include <math.h>

#include "birbs/glm_defs.hh"
#include <glm/ext/vector_float2.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "birbs/core.hh"

namespace birbs {

struct OrthoCamera2d {
	glm::vec2 pos;
	glm::vec2 screen_size;
	float zoom;

	glm::mat4 get_matrix() const {
		glm::vec2 size_over_2 = this->screen_size * this->zoom * 0.5f;
		glm::vec2 bottom_left = pos - size_over_2;
		glm::vec2 top_right = pos + size_over_2;
		return glm::ortho(bottom_left.x, top_right.x, bottom_left.y, top_right.y, -1.0f, 1.0f);
	}
};

inline
float dist2d(glm::vec2 a, glm::vec2 b)
{
	auto dx = a.x - b.x;
	auto dy = a.y - b.y;
	return sqrtf(dx * dx + dy * dy);
}

} // namespace birbs

#endif // MATH_HH
