#ifndef BIRBS_UTIL_HH
#define BIRBS_UTIL_HH

#include "config.h"

#include <stddef.h>
#include <stdint.h>

namespace birbs {

using u8  = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;

using i8  = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;

class NoCopy {
public:
	NoCopy() = default;
	NoCopy(const NoCopy& other) = delete;
	NoCopy &operator=(const NoCopy& other) = delete;
};

class NoMove {
public:
	NoMove() = default;
	NoMove(NoMove &&rhs) noexcept;
	NoMove &operator=(NoMove &&rhs) noexcept;
};

constexpr u8 GL_REQ_VERSION_MAJ = 4;
constexpr u8 GL_REQ_VERSION_MIN = 5; // 2014

namespace units {
	constexpr u32 KB = 1024;
	constexpr u32 MB = KB*1024;
}

#define ARRAY_LEN(x) (sizeof(x) / sizeof(x[0]))

} // namespace birbs

#endif // BIRBS_UTIL_HH
