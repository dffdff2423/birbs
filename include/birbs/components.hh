#ifndef COMPONENTS_HH
#define COMPONENTS_HH

#include "birbs/glm_defs.hh"
#include <glm/ext/vector_float2.hpp>
#include <glm/ext/vector_float3.hpp>

#include "birbs/core.hh"

namespace flecs {
struct world;
} // namespace flecs

namespace birbs {

struct Pos2d {
	glm::vec2 pos;
};

struct Velocity2d {
	glm::vec2 vel;
};

struct Triangle {
	/// Height
	float scale;
};

struct DrawColor {
	glm::vec3 color;
};

void import_components(flecs::world &w);

} // namespace birbs

#endif // COMPONENTS_HH
