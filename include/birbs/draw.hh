#ifndef BIRBS_DRAW_HH
#define BIRBS_DRAW_HH

#include <flecs.h>

#include "birbs/glm_defs.hh"
#include <glm/ext/vector_float2.hpp>
#include <glm/ext/vector_float3.hpp>
#include <glm/ext/matrix_float4x4.hpp>

#include "birbs/components.hh"
#include "birbs/core.hh"
#include "birbs/gl.hh"
#include "birbs/math.hh"
#include "birbs/window.hh"

namespace birbs {

struct DefaultVertex {
       glm::vec3 pos;

       static std::span<const VertexAttribute> get_attrs() {
               constexpr static VertexAttribute ATTRS[] = {
                       { 0, 3, GL_FLOAT, GL_FALSE, offsetof(DefaultVertex, pos)   },
               };

               return std::span(ATTRS, ARRAY_LEN(ATTRS));
       }
};

struct GlobalUniformContents {
       glm::mat4 proj;
};

struct ModelUniformContents {
       /// CONTENTS: xyz = color, w = scale
       glm::vec4 color_and_scale;
       glm::vec2 pos;
       glm::vec2 velo;
};

struct GLDrawInfo {
       VertexBuffer<DefaultVertex> triangle;
       GraphicsShaderProgram prog_centered_sprite;
       UniformBuffer<GlobalUniformContents> global_uniform;
       UniformBuffer<ModelUniformContents> current_sprite_uniform;
};

struct SysPrepFrame {
	void operator()(GLDrawInfo &gl, OrthoCamera2d &cam, const Window &) const;

	static flecs::system add(flecs::world &w)
	{
		return w.system<GLDrawInfo, OrthoCamera2d, const Window>("PrepFrame")
			.kind(flecs::OnStore)
			.term_at(1).singleton()
			.term_at(2).singleton()
			.term_at(3).singleton()
			.each(SysPrepFrame());
	}
};

struct SysDrawMovingTriangleSprites {
	void operator()(GLDrawInfo &info, const Pos2d &pos, const Velocity2d &velo
			, const Triangle &tri, const DrawColor *color) const;

	static flecs::system add(flecs::world &w)
	{
		return w.system<GLDrawInfo, const Pos2d, const Velocity2d, const Triangle
				, const DrawColor *>("DrawMovingTriangleSprites")
			.kind(flecs::OnStore)
			.term_at(1).singleton()
			.each(SysDrawMovingTriangleSprites());
	}
};

struct SysFinishFrame {
	void operator()(const Window &win) const;

	static flecs::system add(flecs::world &w)
	{
		return w.system<const Window>("FinishDrawing")
			.kind(flecs::OnStore)
			.term_at(1).singleton()
			.each(SysFinishFrame());
	}
};

} // namespace birbs

#endif // BIRBS_DRAW_HH
