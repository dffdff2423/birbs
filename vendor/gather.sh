#!/bin/sh

cd "$(dirname "$0")"

FLECS_TAG="v3.2.4"

set -ex

mkdir -p flecs/include
curl "https://raw.githubusercontent.com/SanderMertens/flecs/$FLECS_TAG/flecs.h" -o flecs/include/_flecs.h

mkdir -p flecs/src
curl "https://raw.githubusercontent.com/SanderMertens/flecs/$FLECS_TAG/flecs.c" -o flecs/src/_flecs.c
curl "https://raw.githubusercontent.com/SanderMertens/flecs/$FLECS_TAG/LICENSE" -o flecs/LICENSE
