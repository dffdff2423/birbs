#define FLECS_CUSTOM_BUILD
#define FLECS_CPP
#define FLECS_META
#define FLECS_PIPELINE
#define FLECS_TIMER
#define FLECS_LOG

#ifndef NDEBUG
	#define FLECS_REST
#endif // NDEBUG

#include "_flecs.h"
