#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-function"
#endif

// flecs needs _XOPEN_SOURCE for it's REST extension that is needed for the ECS viewer
#define _XOPEN_SOURCE 600
#include "_flecs.c"

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic pop
#endif
