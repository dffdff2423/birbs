#include <stdlib.h>

#include <chrono>
#include <iostream>
#include <random>
#include <stdexcept>
#include <thread>

#include <flecs.h>

#include <glm/ext/vector_uint2_sized.hpp>

#include "birbs/components.hh"
#include "birbs/draw.hh"
#include "birbs/gl.hh"
#include "birbs/log.hh"
#include "birbs/math.hh"
#include "birbs/window.hh"

constexpr size_t NBOIDS = 200;
constexpr float BOID_START_VELOCITY_MULTIPLIER = 10.0;
constexpr float BOID_TRIANGLE_SCALE = 10.0;
constexpr glm::vec3 BOID_COLOR = glm::vec3(0.9, 0.9, 0.9);

constexpr float VELOCITY_MULTIPLIER = 10.0;

constexpr float OOB_CORRECTION_MODIFIER = 1.0;
constexpr float BOUNDS_BUFFER = 100.0;


constexpr float DIST_TOO_CLOSE = 50.0;
constexpr float AVOID_MODIFIER = 0.5;

constexpr float VIEW_DISTANCE = 125.0;
constexpr float CENTERING_MODIFIER = 0.01;
constexpr float VELOCITY_MATCH_MODIFIER = 0.05;

constexpr float MAX_VELOCITY = 10.0;

namespace birbs {

struct Boid {};

struct sys {};

// BOIDs algorithm as described by: http://www.kfish.org/boids/pseudocode.html

struct SysApplyVelocity {
	void operator()(flecs::iter &it, Pos2d *pos, const Velocity2d *velo) const {
		for (auto i : it) {
			pos[i].pos += velo[i].vel * VELOCITY_MULTIPLIER * it.delta_system_time();
		}
	}

	static flecs::system add(flecs::world &w, flecs::entity src) {
		return w.system<Pos2d, const Velocity2d>("ApplyVelocity")
			.term<Boid>()
			.tick_source(src)
			.kind(flecs::OnUpdate)
			.iter(SysApplyVelocity());
	}
};

struct SysKeepInBounds {
	void operator()(const OrthoCamera2d &cam, const Pos2d &pos, Velocity2d &v) const {
		glm::vec2 cam_extent_over_2 = cam.screen_size * 0.5f;

		float minx = -cam_extent_over_2.x + BOUNDS_BUFFER;
		float maxx =  cam_extent_over_2.x - BOUNDS_BUFFER;
		float miny = -cam_extent_over_2.y + BOUNDS_BUFFER;
		float maxy =  cam_extent_over_2.y - BOUNDS_BUFFER;

		if (pos.pos.x < minx)
			v.vel.x += OOB_CORRECTION_MODIFIER;
		if (pos.pos.x > maxx)
			v.vel.x -= OOB_CORRECTION_MODIFIER;
		if (pos.pos.y < miny)
			v.vel.y += OOB_CORRECTION_MODIFIER;
		if (pos.pos.y > maxy)
			v.vel.y -= OOB_CORRECTION_MODIFIER;
	}

	static flecs::system add(flecs::world &w, flecs::entity src) {
		return w.system<const OrthoCamera2d, const Pos2d, Velocity2d>("KeepInBounds")
			.term<Boid>()
			.tick_source(src)
			.kind(flecs::OnUpdate)
			.term_at(1).singleton()
			.each(SysKeepInBounds());
	}
};


struct SysTorwdsCenterOfBoids {
	void operator()(const Pos2d &pos, Velocity2d &velo) const {
		glm::vec2 center = { 0.0, 0.0 };
		size_t num_in_vision = 0;

		q_others.each([&](const Pos2d &opos, Boid){
			if (dist2d(pos.pos, opos.pos) < VIEW_DISTANCE) {
				center += opos.pos;
				num_in_vision++;
			}
		});

		if (num_in_vision) {
			center = center * (1.0f / num_in_vision);

			velo.vel += (center - pos.pos) * CENTERING_MODIFIER;
		}
	};

	static flecs::system add(flecs::world &w, flecs::entity src) {
		flecs::query others = w.query<const Pos2d, Boid>();

		return w.system<const Pos2d, Velocity2d>("TorwdsCencerOfBoids")
			.term<Boid>()
			.tick_source(src)
			.kind(flecs::OnUpdate)
			.each(SysTorwdsCenterOfBoids { std::move(others) });
	}

	flecs::query<const Pos2d, Boid> q_others;
};

struct SysAvoidOthers {
	void operator()(flecs::iter &it, const Pos2d *pos, Velocity2d *velo) const {
		for (auto i : it) {
			auto me = it.entity(i);
			glm::vec2 vmod = {0.0, 0.0};
			q_others.iter([&](flecs::iter &o_it, const Pos2d *o_pos, Boid *) {
				for (auto o : o_it) {
					auto other = o_it.entity(o);
					if (other == me)
						continue;
					float d = dist2d(pos[i].pos, o_pos[o].pos);
					if (d < DIST_TOO_CLOSE) {
						vmod += (pos[i].pos - o_pos[o].pos) * (1.0f/d);
					}
				}
			});
			velo[i].vel += vmod * AVOID_MODIFIER;
		}
	}

	static flecs::system add(flecs::world &w, flecs::entity src) {
		flecs::query others = w.query<const Pos2d, Boid>();

		return w.system<const Pos2d, Velocity2d>("AvoidOthers")
			.term<Boid>()
			.tick_source(src)
			.kind(flecs::OnUpdate)
			.iter(SysAvoidOthers { std::move(others) });
	}

	flecs::query<const Pos2d, Boid> q_others;
};

struct SysMatchVelocity {
	void operator()(const Pos2d pos, Velocity2d &velo) const {
		glm::vec2 vacc = { 0.0, 0.0 };
		size_t n = 0;

		q_others.each([&](const Pos2d o_pos, const Velocity2d &other, Boid){
			if (dist2d(pos.pos, o_pos.pos) < VIEW_DISTANCE) {
				vacc += other.vel;
				n++;
			}
		});

		if (n > 0) {
			vacc *= (1.0f/(n-1));

			velo.vel += vacc * VELOCITY_MATCH_MODIFIER;
		}
	}

	static flecs::system add(flecs::world &w, flecs::entity src) {
		flecs::query others = w.query<const Pos2d, const Velocity2d, Boid>();

		return w.system<const Pos2d, Velocity2d>("MatchVelocity")
			.term<Boid>()
			.tick_source(src)
			.kind(flecs::OnUpdate)
			.each(SysMatchVelocity { std::move(others) });
	}

	flecs::query<const Pos2d, const Velocity2d, Boid> q_others;
};

struct SysCapSpeed {
	void operator()(Velocity2d &v) const {
		if (glm::length(v.vel) > MAX_VELOCITY) {
			v.vel = (v.vel / glm::length(v.vel)) * MAX_VELOCITY;
		}
	}

	static flecs::system add(flecs::world &w, flecs::entity src) {
		return w.system<Velocity2d>("CapSpeed")
			.term<Boid>()
			.tick_source(src)
			.kind(flecs::OnUpdate)
			.each(SysCapSpeed());
	}
};

} // namespace birbs

static
int try_main(void)
{
	auto wctx = birbs::WindowingCTX::init();
	auto win = birbs::Window(wctx, {1280, 720}, "birbs");

	std::array<birbs::DefaultVertex, 3> triangle_verts = {{
		{{-0.5f, -0.5f, 0.0f}},
		{{ 0.5f, -0.5f, 0.0f}},
		{{ 0.0f,  0.8f, 0.0f}},
	}};
	auto triangle = birbs::VertexBuffer<birbs::DefaultVertex>(triangle_verts);
	auto sprite_shader = birbs::GraphicsShaderProgram("shaders/centered_sprite.vert.spv", "shaders/basic.frag.spv");
	birbs::OrthoCamera2d cam = {
		.pos = { 0.0, 0.0 },
		.screen_size = win.size(),
		.zoom = 1.0,
	};
	auto global_uniform = birbs::UniformBuffer<birbs::GlobalUniformContents>();
	{
		auto u = global_uniform.map(GL_WRITE_ONLY);
		u->proj = cam.get_matrix();
	}

	auto local_uniform = birbs::UniformBuffer<birbs::ModelUniformContents>();
	{
		auto u = local_uniform.map(GL_WRITE_ONLY);
		u->color_and_scale = glm::vec4(1.0);
		u->pos = glm::vec2(0.0);
		u->velo = glm::vec2(0.0);
	}

	auto gl_info = birbs::GLDrawInfo {
		.triangle = std::move(triangle),
		.prog_centered_sprite = std::move(sprite_shader),
		.global_uniform = std::move(global_uniform),
		.current_sprite_uniform = std::move(local_uniform),
	};

	auto world = flecs::world();
	world.set_threads(std::thread::hardware_concurrency());
#ifndef NDEBUG
	world.set<flecs::Rest>({});
#endif // NDEBUG
	birbs::import_components(world);
	world.set<birbs::OrthoCamera2d>(cam);
	world.emplace<birbs::GLDrawInfo>(std::move(gl_info));
	world.emplace<birbs::Window>(std::move(win));

	flecs::timer ticker = world.timer("birbs::UpdateTicker")
		.interval(1/30.0);
	ticker.start();

	world.scope<birbs::sys>([&]() {
		world.system<const birbs::Window>("PollWindow")
			.kind(flecs::OnLoad)
			.term_at(1).singleton()
			.each([&](const birbs::Window &w) { w.poll(); });

		birbs::SysTorwdsCenterOfBoids::add(world, ticker);
		birbs::SysAvoidOthers::add(world, ticker);
		birbs::SysMatchVelocity::add(world, ticker);
		birbs::SysKeepInBounds::add(world, ticker);
		birbs::SysCapSpeed::add(world, ticker);
		birbs::SysApplyVelocity::add(world, ticker);

		birbs::SysPrepFrame::add(world);
		birbs::SysDrawMovingTriangleSprites::add(world);
		birbs::SysFinishFrame::add(world);
	});

	const birbs::Window *w  = world.get<birbs::Window>();
	auto sz = glm::vec2(w->size());

	world.defer_begin();

	std::random_device rng_src = {};
	std::default_random_engine rng {rng_src()};
	auto norm_dist = std::uniform_real_distribution<float>(0.0, 1);
	for (size_t i = 0; i < NBOIDS; ++i) {
		auto x = norm_dist(rng) * (float)sz.x - ((float)sz.x / 2.0);
		auto y = norm_dist(rng) * (float)sz.y - ((float)sz.y / 2.0);
		auto dx = norm_dist(rng) * BOID_START_VELOCITY_MULTIPLIER - BOID_START_VELOCITY_MULTIPLIER / 2.0;
		auto dy = norm_dist(rng) * BOID_START_VELOCITY_MULTIPLIER - BOID_START_VELOCITY_MULTIPLIER / 2.0;

		flecs::entity boid = world.entity();
		boid.set<birbs::Pos2d>({{ x, y }});
		boid.set<birbs::Velocity2d>({{ dx, dy }});
		boid.set<birbs::Triangle>({ BOID_TRIANGLE_SCALE });
		boid.set<birbs::DrawColor>({ BOID_COLOR });
		boid.add<birbs::Boid>();
	}

	world.defer_end();

	world.set_target_fps(60.0); // FIXME

	while (!w->should_close()) {
		world.progress();
	}
	return 0;
}

int main(void)
{
	try {
		try_main();
	} catch (std::exception &e) {
		LOG_e("FATAL: {}", e.what());
	}
	return 0;
}
