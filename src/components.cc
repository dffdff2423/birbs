#include "birbs/components.hh"

#include <flecs.h>

#include "birbs/glm_defs.hh"
#include <glm/ext/vector_float2.hpp>
#include <glm/ext/vector_float3.hpp>
#include <glm/ext/vector_float4.hpp>

namespace birbs {

struct comp {};

void import_components(flecs::world &w)
{
	w.component<glm::vec2>("glm::vec2")
		.member<float>("x")
		.member<float>("y");

	w.component<glm::vec3>("glm::vec3")
		.member<float>("x")
		.member<float>("y")
		.member<float>("z");

	w.component<glm::vec4>("glm::vec4")
		.member<float>("x")
		.member<float>("y")
		.member<float>("z")
		.member<float>("w");

	w.scope<comp>([&](){
		w.component<Pos2d>()
			.member<glm::vec2>("pos");

		w.component<Velocity2d>()
			.member<glm::vec2>("vel");

		w.component<Triangle>()
			.member<float>("scale");

		w.component<DrawColor>()
			.member<glm::vec3>("color");
	});
}

} // namespace birbs

