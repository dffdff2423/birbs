#include "birbs/draw.hh"

#include "birbs/components.hh"
#include "birbs/math.hh"
#include "birbs/window.hh"

namespace birbs {

void SysDrawMovingTriangleSprites::operator()(GLDrawInfo &info, const Pos2d &pos
		, const Velocity2d &velo, const Triangle &tri, const DrawColor *_color) const
{
	{
		auto color = _color ? _color->color : glm::vec3(1.0);
		auto u = info.current_sprite_uniform.map(GL_WRITE_ONLY);
		u->color_and_scale = glm::vec4(color, tri.scale);
		u->pos = pos.pos;
		u->velo = velo.vel;
	}
	info.global_uniform.bind_to(0);
	info.current_sprite_uniform.bind_to(1);
	draw_triangles(info.triangle, info.prog_centered_sprite);
}

void SysPrepFrame::operator()(GLDrawInfo &gl, OrthoCamera2d &cam, const Window &win) const
{
	glClearColor(0.2, 0.3, 0.3, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	cam.screen_size = win.size();
	{
		auto u = gl.global_uniform.map(GL_WRITE_ONLY);
		u->proj = cam.get_matrix();
	}
}

void SysFinishFrame::operator()(const Window &win) const
{
	win.swap_buffers();
}

} // namespace birbs
