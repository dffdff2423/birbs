#include "birbs/gl.hh"

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include <array>
#include <format>
#include <stdexcept>
#include <system_error>
#include <vector>

#include <glad/gl.h>
#include <GLFW/glfw3.h>

#ifdef HAS_LIBUNWIND
#define UNW_LOCAL_ONLY
#include <cxxabi.h>
#include <libunwind.h>
#endif // HAS_LIBUNWIND

#include "birbs/log.hh"

namespace birbs {

static void APIENTRY gl_debug_callback(GLenum src, GLenum type, unsigned id
		, GLenum severity, GLsizei len , const char *message, const void *user);

void load_gl()
{
	int version = gladLoadGL(glfwGetProcAddress);
	if (version == 0) {
		throw std::runtime_error("Failed to load opengl");
	}
	if (GLAD_VERSION_MAJOR(version) < GL_REQ_VERSION_MAJ
			|| GLAD_VERSION_MINOR(version) < GL_REQ_VERSION_MIN) {
		auto txt = std::format("Failed to load supported opoengl version. Want: {}.{}; Got: {}.{}"
				, GL_REQ_VERSION_MAJ, GL_REQ_VERSION_MIN
				, GLAD_VERSION_MAJOR(version), GLAD_VERSION_MINOR(version));
		throw std::runtime_error(txt);
	}
	if (!GLAD_GL_ARB_gl_spirv)
		throw std::runtime_error("OS does not support required opengl extension: GL_ARB_gl_spirv. "
				"If you see this message, please let the developers know what GPU you are using");
#ifndef NDEBUG
	if (GLAD_GL_KHR_debug) {
		GLint flags = 0;
		glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
		if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
			glEnable(GL_DEBUG_OUTPUT);
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
			glDebugMessageCallback(gl_debug_callback, nullptr);
			glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, true);
		} else {
			LOG_w("Debug build but not debug context. Opengl debug callback disabled.");
		}
	} else {
		LOG_w("Debug build but GPU does not have GL_KHR_debug.");
	}
#endif
}

[[maybe_unused]] constexpr size_t MAX_BACKTRACE_NAME_SZ = 512;

static
unsigned long get_file_size(FILE *f)
{
	if (fseek(f, 0L, SEEK_END) < 0) {
		throw std::system_error(errno, std::generic_category(), "Failed to size file (fseek)");
	}
	long sz = ftell(f);
	if (sz < 0) {
		throw std::system_error(errno, std::generic_category(), "Failed to size file (ftell)");
	}
	rewind(f);
	return sz;
}

static
std::vector<u8> read_file(const char * path)
{
	std::vector<u8> txt = {};
	static_assert(sizeof(u8) == 1);

	FILE *f = fopen(path, "rb");
	if (f == nullptr)
		throw std::system_error(errno, std::generic_category()
				, std::format("Failed to read file {}", path));

	try {
		unsigned long fsize = get_file_size(f);
		txt.resize(fsize);
		size_t read = 0;
		while (read < fsize && !feof(f)) {
			size_t res = fread(static_cast<void *>(txt.data() + read), 1, fsize, f);
			if (ferror(f)) {
				throw std::system_error(errno, std::generic_category(), "");
			}
			read += res;
		}
	} catch(...) {
		fclose(f);
		throw;
	}

	fclose(f);
	return txt;
}

static
bool check_shader_success(GLuint handle)
{
	GLint is_ok = 0;
	glGetShaderiv(handle, GL_COMPILE_STATUS, &is_ok);
	if (!is_ok) {
		LOG_e("Failed to compile shader");
		GLint info_log_len;
		glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &info_log_len);

		auto log = std::vector<GLchar>(info_log_len);
		glGetShaderInfoLog(handle, info_log_len, nullptr, log.data());
		log.push_back('\0');
		fputs(std::format("Info Log:\n{}", log.data()).c_str(), stderr);
	}
	return is_ok;
}

GraphicsShaderProgram::GraphicsShaderProgram(const char *vsh_path, const char *fsh_path)
{
	std::vector<u8> vsh_src = read_file(vsh_path);
	std::vector<u8> fsh_src = read_file(fsh_path);
	GLuint vert_handle = glCreateShader(GL_VERTEX_SHADER);
	GLuint frag_handle = glCreateShader(GL_FRAGMENT_SHADER);
	m_handle = glCreateProgram();

	try {
		glShaderBinary(1, &vert_handle, GL_SHADER_BINARY_FORMAT_SPIR_V
				, vsh_src.data(), vsh_src.size() * sizeof(u8));
		glSpecializeShaderARB(vert_handle, "main", 0, nullptr, nullptr);
		if (!check_shader_success(vert_handle))
			throw std::runtime_error(std::format("Failed to compile shader: {}", vsh_path));

		glShaderBinary(1, &frag_handle, GL_SHADER_BINARY_FORMAT_SPIR_V
				, fsh_src.data(), fsh_src.size() * sizeof(u8));
		glSpecializeShaderARB(frag_handle, "main", 0, nullptr, nullptr);
		if (!check_shader_success(frag_handle))
			throw std::runtime_error(std::format("Failed to compile shader: {}", fsh_path));

		glAttachShader(m_handle, vert_handle);
		glAttachShader(m_handle, frag_handle);
		glLinkProgram(m_handle);

		GLint is_ok = 0;
		glGetProgramiv(m_handle, GL_LINK_STATUS, &is_ok);
		if (!is_ok) {
			LOG_e("Failed to link shader program");
			GLint info_log_len;
			glGetProgramiv(m_handle, GL_INFO_LOG_LENGTH, &info_log_len);

			auto log = std::vector<GLchar>(info_log_len);
			glGetProgramInfoLog(m_handle, info_log_len, nullptr, log.data());
			log.push_back('\0');
			fputs(std::format("Info Log:\n{}", log.data()).c_str(), stderr);

			auto txt = std::format("Failed to link shader program (vsh = {}, fsh = {})"
					, vsh_path, fsh_path);
			throw std::runtime_error(txt);
		}
	} catch (...) {
		glDeleteShader(vert_handle);
		glDeleteShader(frag_handle);
		glDeleteProgram(m_handle);
		throw;
	}

	glDeleteShader(vert_handle);
	glDeleteShader(frag_handle);

	LOG_d("Shader pair `{}`, `{}` compiled successfully", vsh_path, fsh_path);
}

GraphicsShaderProgram::~GraphicsShaderProgram() noexcept
{
	glDeleteProgram(m_handle);
}

void GraphicsShaderProgram::use() const noexcept
{
	glUseProgram(m_handle);
}

#ifdef HAS_LIBUNWIND

static void print_backtrace()
{
#define RESCHECK do { \
	if (res < 0) { \
		fputs("Failed to get backtrace", stderr); \
		return; \
	} } while (0) \


	unw_cursor_t cursor;
	unw_context_t ctx;
	int res = unw_getcontext(&ctx);
	RESCHECK;
	res = unw_init_local(&cursor, &ctx);
	RESCHECK;

	int pos = 0;
	for (;;) {
		int step = unw_step(&cursor);
		res = step;
		RESCHECK;

		unw_word_t ip, sp;

		res = unw_get_reg(&cursor, UNW_REG_IP, &ip);
		RESCHECK;
		res = unw_get_reg(&cursor, UNW_REG_SP, &sp);
		RESCHECK;

		unw_word_t off;
		char symb[MAX_BACKTRACE_NAME_SZ] = { "<unknown>" };
		char *name = symb;

		if (!unw_get_proc_name(&cursor, symb, sizeof(symb), &off)) {
			int stat;
			if ((name = abi::__cxa_demangle(symb, nullptr, nullptr, &stat)) == nullptr) {
				name = symb;
			}
		}
		fprintf(stderr, "#%-2d 0x%016" PRIxPTR " sp=0x%016" PRIxPTR " %s + 0x%" PRIxPTR "\n"
				, pos++, static_cast<uintptr_t>(ip)
				, static_cast<uintptr_t>(sp), name
				, static_cast<uintptr_t>(off));

		if (step == 0)
			break;
		if (name != symb)
			free(name);
	}

#undef RESCHECK
}

#endif // HAS_LIBUNWIND

static
void APIENTRY gl_debug_callback(GLenum src, GLenum type, unsigned id
		, GLenum severity, GLsizei len , const char *message, const void *user)
{
	(void)len;
	(void)user;

	// ignore non-significant error/warning codes, taken form: https://learnopengl.com/In-Practice/Debugging
	if(id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

	const char *src_txt;
	switch (src) {
	case GL_DEBUG_SOURCE_API:             src_txt = "API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   src_txt = "Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: src_txt = "Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:     src_txt = "Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:     src_txt = "Application"; break;
	case GL_DEBUG_SOURCE_OTHER:           src_txt = "Other"; break;
	default:                              src_txt = "UNKNOWN SOURCE"; break;
	}

	const char *type_txt;
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               type_txt = "Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: type_txt = "Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  type_txt = "Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         type_txt = "Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         type_txt = "Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              type_txt = "Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          type_txt = "Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           type_txt = "Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               type_txt = "Other"; break;
	default:                                type_txt = "UNKNOWN TYPE"; break;
	}

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:
		LOG_e("SRC: {}; TYPE: {}; MESSAGE:\n\t{}", src_txt, type_txt, message); break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		LOG_w("SRC: {}; TYPE: {}; MESSAGE:\n\t{}", src_txt, type_txt, message); break;
	case GL_DEBUG_SEVERITY_LOW:
		LOG_i("SRC: {}; TYPE: {}; MESSAGE:\n\t{}", src_txt, type_txt, message); break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		LOG_d("SRC: {}; TYPE: {}; MESSAGE:\n\t{}", src_txt, type_txt, message); break;
	default:
		LOG_e("Unknown severity Gl message: SRC: {}; TYPE: {}; MESSAGE:\n\t{}", src_txt, type_txt, message);
	}

#ifdef HAS_LIBUNWIND
	fprintf(stderr, "Backtrace:\n");
	print_backtrace();
#endif // HAS_LIBUNWIND
}

} // namespace birbs
