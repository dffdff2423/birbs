#include "birbs/window.hh"

#include <atomic>
#include <format>

#include <glad/gl.h>
#include <GLFW/glfw3.h>

#include "birbs/log.hh"
#include "birbs/gl.hh"

namespace birbs {

static
void glfw_error_callback(int code, const char *descripion)
{
	(void)code;
	LOG_e("{}", descripion);
}

static
void glfw_framebuffer_size_callback(GLFWwindow *win, int wid, int height)
{
	(void)win;
	(void)wid;
	(void)height;
	glViewport(0, 0, wid, height);
}

static
void glfw_key_callback(GLFWwindow *_win, int key, int scancode, int action, int mods)
{
	(void)scancode;
	(void)mods;

	auto *win = static_cast<Window *>(glfwGetWindowUserPointer(_win));
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		win->set_close();
}

WindowingCTX WindowingCTX::init()
{
	glfwSetErrorCallback(glfw_error_callback);

	if (glfwInit() == GLFW_FALSE) {
		const char *txt;
		int err = glfwGetError(&txt);
		throw GLFWError(err, txt);
	}

	return WindowingCTX {};
}

WindowingCTX::~WindowingCTX()
{
	glfwTerminate(); // NOTE: we ignore errors here
}

Window::Window(WindowingCTX &, glm::u32vec2 size, const char *title)
{
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, GL_REQ_VERSION_MAJ);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, GL_REQ_VERSION_MIN);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHintString(GLFW_X11_CLASS_NAME, "birbs");

#ifndef NDEBUG
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
#endif

	m_handle = glfwCreateWindow(size.x, size.y, title, nullptr, nullptr);
	if (m_handle == nullptr) {
		const char *descr;
		int code = glfwGetError(&descr);
		throw GLFWError(code, descr);
	}
	this->make_current();
	load_gl();
	glViewport(0, 0, size.x, size.y);

	set_callback_ptr();
	glfwSetFramebufferSizeCallback(m_handle, glfw_framebuffer_size_callback);
	glfwSetKeyCallback(m_handle, glfw_key_callback);
}

Window::~Window()
{
	if (m_handle == nullptr)
		return;
	glfwDestroyWindow(m_handle);
}

bool Window::should_close() const noexcept
{
	return glfwWindowShouldClose(m_handle);
}

void Window::set_close(bool val) const noexcept
{
	glfwSetWindowShouldClose(m_handle, val);
}

void Window::make_current() const noexcept
{
	glfwMakeContextCurrent(m_handle);
}

void Window::swap_buffers() const noexcept
{
	glfwSwapBuffers(m_handle);
}

void Window::poll() const noexcept
{
	glfwPollEvents();
}

glm::uvec2 Window::size() const noexcept
{
	int w, h;
	glfwGetWindowSize(m_handle, &w, &h);
	return {w, h};
}

void Window::set_callback_ptr() noexcept
{
	glfwSetWindowUserPointer(m_handle, this);
}

} // namespace birbs

